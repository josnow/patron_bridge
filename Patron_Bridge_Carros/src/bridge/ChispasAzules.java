package bridge;

public class ChispasAzules implements IColor {

	@Override
	public void procesar() {

		System.out.println("\tPintura en forma de chispas azules");
	}

}
