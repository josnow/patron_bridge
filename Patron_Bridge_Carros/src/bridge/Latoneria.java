package bridge;

public abstract class Latoneria {

	protected IColor color;
	
	protected Latoneria(IColor color) {
		this.color = color;
	}
	
	public void procesar() {
		color.procesar();
	}

	public IColor getColor() {
		return color;
	}

	public void setColor(IColor color) {
		this.color = color;
	}
	
}
