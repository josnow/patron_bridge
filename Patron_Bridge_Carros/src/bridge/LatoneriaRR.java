package bridge;

public class LatoneriaRR extends Latoneria {

	protected LatoneriaRR(IColor color) {
		super(color);
		System.out.println("Latoneria de Rolls Royce\n");
	}

}
