package bridge;

public class LatoneriaPorshe extends Latoneria {

	protected LatoneriaPorshe(IColor color) {
		super(color);
		System.out.println("Latoneria de Porshe\n");
	}

}
