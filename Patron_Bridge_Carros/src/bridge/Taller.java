package bridge;

public abstract class Taller {
	
	protected IAdornoDeCapo adorno;

	protected Taller(IAdornoDeCapo adorno) {
		
		this.adorno = adorno;
	}
	
	public void procesar() {
		adorno.procesar();
	}

	public IAdornoDeCapo getAdorno() {
		return adorno;
	}

	public void setAdorno(IAdornoDeCapo adorno) {
		this.adorno = adorno;
	}
	
}
