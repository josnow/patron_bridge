package vista;

import java.util.Scanner;

import bridge.*;


public class User {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);
		
		Latoneria latoneria = new LatoneriaBMW(new RayosRojos());
		latoneria.procesar();
		
		latoneria.setColor(new ChispasAzules());
		latoneria.procesar();
		
		Taller taller = new TallerPorshe(new AdornoDeRayo());
		taller.procesar();
		
		taller.setAdorno(new AdornoCaballito());
		taller.procesar();
	}

}
